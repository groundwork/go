<?php

//remove profile links
function remove_xprofile_links() {
    remove_filter( 'bp_get_the_profile_field_value', 'xprofile_filter_link_profile_data', 9, 2 );
}
add_action( 'bp_init', 'remove_xprofile_links' );

//change tab order
function tricks_change_bp_tag_position()
{
global $bp;
$bp->bp_nav['profile']['position'] = 10;
$bp->bp_nav['friends']['position'] = 30;
$bp->bp_nav['settings']['position'] = 40;
}
add_action( 'bp_init', 'tricks_change_bp_tag_position', 999 );

//change profile to start tab
define ( 'BP_DEFAULT_COMPONENT', 'profile' );
 
//remove buddypress adminbar items
function remove_adminbar_items() {
    remove_action( 'bp_adminbar_menus', 'bp_adminbar_random_menu', 100 );
    //remove_action( 'bp_adminbar_logo', 'bp_adminbar_logo' );
}
add_action( 'bp_init', 'remove_adminbar_items' );

//redirect logged in users
function register_page_redirect( $redirect_to ) {
    if ( bp_is_page( 'register' ) )
        $redirect_to = bp_get_loggedin_user_link();

    return $redirect_to;
}
add_filter( 'bp_loggedin_register_page_redirect_to', 'register_page_redirect' );

/*add help link
function add_help_button() {
	echo '<li id="bp-adminbar-help-menu"><a href="http://help.groundworkopportunities.org">Help</a></li>';
}
add_action('bp_adminbar_menus', 'add_help_button', 10);*/

?>